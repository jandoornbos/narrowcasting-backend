package com.stenden.narrowcasting.configuration;

import com.stenden.narrowcasting.model.Setting;
import com.stenden.narrowcasting.repository.EventRepository;
import com.stenden.narrowcasting.service.*;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.stenden.narrowcasting.controller")
public class TestContext {

    @Bean
    public MessageService messageServiceMock() {
        return Mockito.mock(MessageService.class);
    }

    @Bean
    public AuthenticationService authenticationService() {
        return Mockito.mock(AuthenticationService.class);
    }

    @Bean
    public UserService userService() {
        return Mockito.mock(UserService.class);
    }

    @Bean
    public EventService eventService() {
        return Mockito.mock(EventService.class);
    }

    @Bean
    public TeacherService teacherService() {
        return Mockito.mock(TeacherService.class);
    }

    @Bean
    public SettingService settingService() {
        return Mockito.mock(SettingService.class);
    }

    @Bean
    public EventRepository eventRepository(){return Mockito.mock(EventRepository.class);}

}
