package com.stenden.narrowcasting.util;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class PasswordTest {

    @Test
    public void decryptPassword_shouldReturnTrue() throws Exception {
        String plainPassword = "test";
        String hashedPassword = Password.hashPassword(plainPassword);

        assertTrue(Password.checkPassword(plainPassword, hashedPassword));
    }

    @Test
    public void encryptPassword_shouldBeUnreadable() throws Exception {
        String plainPassword = "test";
        String hashedPassword = Password.hashPassword(plainPassword);

        assertTrue(!plainPassword.equalsIgnoreCase(hashedPassword));
    }

}
