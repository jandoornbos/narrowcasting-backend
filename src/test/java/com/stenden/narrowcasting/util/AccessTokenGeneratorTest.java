package com.stenden.narrowcasting.util;


import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AccessTokenGeneratorTest {

    @Test
    public void createToken_shouldReturnString() throws Exception {
        String token = new AccessTokenGenerator().nextSessionId();
        assertTrue(token.length() == 26);
    }

}
