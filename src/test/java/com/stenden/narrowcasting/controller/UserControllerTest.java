package com.stenden.narrowcasting.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stenden.narrowcasting.configuration.TestContext;
import com.stenden.narrowcasting.configuration.WebContext;
import com.stenden.narrowcasting.exception.UserNotFoundException;
import com.stenden.narrowcasting.model.User;
import com.stenden.narrowcasting.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestContext.class, WebContext.class })
@WebAppConfiguration
public class UserControllerTest {

    private MockMvc mockMvc;
    ObjectMapper objectMapper =  new ObjectMapper();

    @Autowired
    private UserService userServiceMock;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        Mockito.reset(this.userServiceMock);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    /**
     * Get valid users.
     * @result HTTP Status 200 Ok. Retrieved correctly.
     */
    @Test
    public void getUsers() throws Exception {
        User user1 = new User();
        user1.setId(1);
        user1.setUsername("username 1.");
        user1.setScreenname("username 1.");
        user1.setPassword("password");

        when(this.userServiceMock.getUsers()).thenReturn(Arrays.asList(user1));

        this.mockMvc.perform(get("/api/v1/user"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].username", is("username 1.")))
                .andExpect(jsonPath("$[0].screenname", is("username 1.")));
    }

    /**
     * Entered wrong id.
     * @throws Exception
     */
    @Test(expected = UserNotFoundException.class)
    public void getUsers_shouldThrowExceptionWhenWrongUsernameIsEntered() throws Exception{
        doThrow(new UserNotFoundException()).when(userServiceMock).getUserByUsername("testUsername");

        userServiceMock.getUserByUsername("testUsername");
    }

    /**
     * Entered wrong id.
     * @throws Exception
     */
    @Test(expected = UserNotFoundException.class)
    public void getUsers_shouldThrowExceptionWhenWrongIdIsEntered() throws Exception{
        doThrow(new UserNotFoundException()).when(userServiceMock).getSingleUser(3);

        userServiceMock.getSingleUser(3);
    }

    /**
     * Create a valid user.
     * @result HTTP Status 200 Ok. Created correctly.
     */
    @Test
    public void registerUser() throws Exception {
        User user1 = new User();
        user1.setId(1);
        user1.setUsername("username 1.");
        user1.setScreenname("username 1.");
        user1.setPassword("password");

        String json = objectMapper.writeValueAsString(user1);
        when(this.userServiceMock.createUser(user1)).thenReturn(user1);

        mockMvc.perform(post("/api/v1/user")
                .contentType(APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
    }

    /**
     * Get a valid user.
     * @result HTTP Status 200 Ok. Retrieved correctly.
     */
    @Test
    public void getSingleUser() throws Exception {
        User user1 = new User();
        user1.setId(1);
        user1.setUsername("username 1.");
        user1.setScreenname("username 1.");
        user1.setPassword("password");

        when(this.userServiceMock.getSingleUser(1)).thenReturn(user1);

        this.mockMvc.perform(get("/api/v1/user/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.username", is("username 1.")))
                .andExpect(jsonPath("$.screenname", is("username 1.")));
    }


    /**
     * Update a valid user.
     * @result HTTP Status 200 Ok. Updated correctly.
     */
    @Test
    public void updateUser() throws Exception {
        User user1 = new User();
        user1.setId(1);
        user1.setUsername("username 1.");
        user1.setScreenname("username 1.");
        user1.setPassword("password");

        String json = objectMapper.writeValueAsString(user1);
        when(this.userServiceMock.updateUser(1, user1)).thenReturn(user1);

        mockMvc.perform(put("/api/v1/user/1")
                .contentType(APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());
    }

    /**
     * Entered wrong id.
     * @throws Exception
     */
    @Test(expected = UserNotFoundException.class)
    public void updateUser_shouldThrowExceptionWhenWrongIdIsEntered() throws Exception{
        User user1 = new User();
        user1.setId(1);
        user1.setUsername("username 1.");
        user1.setScreenname("username 1.");
        user1.setPassword("password");

        doThrow(new UserNotFoundException()).when(userServiceMock).updateUser(4, user1);

        userServiceMock.updateUser(4, user1);
    }

    /**
     * Delete a valid user.
     * @result HTTP Status 200 Ok. Updated correctly.
     */
    @Test
    public void deleteUser() throws Exception {
        User user1 = new User();
        user1.setId(1);
        user1.setUsername("username 1.");
        user1.setScreenname("username 1.");
        user1.setPassword("password");

        this.userServiceMock.deleteUser(1);

        this.mockMvc.perform(delete("/api/v1/user/1"))
                .andExpect(status().isOk());
    }

    /**
     * User not found.
     * @throws Exception
     */
    @Test(expected = UserNotFoundException.class)
    public void deleteUser_shouldThrowExceptionWhenWrongIdIsEntered() throws Exception{
        doThrow(new UserNotFoundException()).when(userServiceMock).deleteUser(3);

        userServiceMock.deleteUser(3);
    }
}