package com.stenden.narrowcasting.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stenden.narrowcasting.configuration.TestContext;
import com.stenden.narrowcasting.configuration.WebContext;
import com.stenden.narrowcasting.exception.DateNotValidException;
import com.stenden.narrowcasting.exception.EventNotFoundException;
import com.stenden.narrowcasting.exception.MessageNotFoundException;
import com.stenden.narrowcasting.model.Message;
import com.stenden.narrowcasting.service.MessageService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestContext.class, WebContext.class })
@WebAppConfiguration
public class MessageControllerTest {

    private MockMvc mockMvc;
    ObjectMapper objectMapper =  new ObjectMapper();

    @Autowired
    private MessageService messageServiceMock;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        Mockito.reset(this.messageServiceMock);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    /**
     * Get valid messages.
     * @result HTTP Status 200 Ok. Retrieved correctly.
     */
    @Test
    public void findAll_shouldReturnAllMessages() throws Exception {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss ");
        Date currentTime = new Date();
        Date date2 =  new Date(currentTime.getTime() + (1000 * 60 * 60 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 60 * 60 * 24));

        Message message1 = new Message();
        message1.setTitle("Test");
        message1.setContent("This is a test message.");
        message1.setCreationDate(date3);
        message1.setExpirationDate(date2);
        message1.setId(1);

        when(this.messageServiceMock.getMessages()).thenReturn(Arrays.asList(message1, message1));

        this.mockMvc.perform(get("/api/v1/message"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].title", is("Test")))
                .andExpect(jsonPath("$[0].creationDate", is(date3.getTime())))
                .andExpect(jsonPath("$[0].expirationDate", is(date2.getTime())))
                .andExpect(jsonPath("$[0].content", is("This is a test message.")));
    }

    /**
     * Get a valid message.
     * @result HTTP Status 200 Ok. Retrieved correctly.
     */
    @Test
    public void findOne_shouldReturnOneMessage() throws Exception {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss ");
        Date currentTime = new Date();
        Date date2 =  new Date(currentTime.getTime() + (1000 * 60 * 60 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 60 * 60 * 24));

        Message message1 = new Message();
        message1.setTitle("Test");
        message1.setContent("This is a test message.");
        message1.setCreationDate(date3);
        message1.setExpirationDate(date2);
        message1.setId(1);

        when(this.messageServiceMock.getSingleMessage(1)).thenReturn(message1);

        this.mockMvc.perform(get("/api/v1/message/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.title", is("Test")))
                .andExpect(jsonPath("$.creationDate", is(date3.getTime())))
                .andExpect(jsonPath("$.expirationDate", is(date2.getTime())))
                .andExpect(jsonPath("$.content", is("This is a test message.")));
    }

    /**
     * Create a valid message.
     * @result HTTP Status 201 Ok. Created correctly.
     */
    @Test
    public void saveMessage() throws Exception {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss ");
        Date currentTime = new Date();
        Date date2 =  new Date(currentTime.getTime() + (1000 * 3600 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 3600 * 24));

        Message message1 = new Message();
        message1.setTitle("Test");
        message1.setContent("This is a test message.");
        message1.setCreationDate(date3);
        message1.setExpirationDate(date2);
        message1.setId(1);

        String json = objectMapper.writeValueAsString(message1);
        when(this.messageServiceMock.saveMessage(message1)).thenReturn(message1);

        mockMvc.perform(post("/api/v1/message")
                .contentType(APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
    }

    /**
     * Creation date higher then event date.
     * @throws Exception
     */
    @Test(expected = DateNotValidException.class)
    public void saveMessage__shouldNotHaveCreationDateHigherThanEventDateAndShouldThrowException() throws Exception{
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss ");
        Date currentTime = new Date();
        Date date2 =  new Date(currentTime.getTime() + (1000 * 3600 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 3600 * 24));

        Message message1 = new Message();
        message1.setTitle("Test");
        message1.setContent("This is a test message.");
        message1.setCreationDate(date2);
        message1.setExpirationDate(date3);
        message1.setId(1);

        doThrow(new DateNotValidException()).when(messageServiceMock).saveMessage(message1);

        messageServiceMock.saveMessage(message1);
    }

    /**
     * Update a valid message.
     * @result HTTP Status 200 Ok. Updated correctly.
     */
    @Test
    public void updateMessage() throws Exception {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss ");
        Date currentTime = new Date();
        Date date2 =  new Date(currentTime.getTime() + (1000 * 60 * 60 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 60 * 60 * 24));

        Message message1 = new Message();
        message1.setTitle("Test");
        message1.setContent("This is a test message.");
        message1.setCreationDate(date3);
        message1.setExpirationDate(date2);
        message1.setId(1);

        String json = objectMapper.writeValueAsString(message1);
        when(this.messageServiceMock.updateMessage(1, message1)).thenReturn(message1);

        mockMvc.perform(patch("/api/v1/message/1")
                .contentType(APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());
    }

    /**
     * Wrong id entered.
     * @throws Exception
     */
    @Test(expected = MessageNotFoundException.class)
    public void updateMessage_shouldThrowExceptionWhenWrongIdIsEntered() throws Exception{
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss ");
        Date currentTime = new Date();
        Date date2 =  new Date(currentTime.getTime() + (1000 * 60 * 60 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 60 * 60 * 24));

        Message message1 = new Message();
        message1.setTitle("Test");
        message1.setContent("This is a test message.");
        message1.setCreationDate(date3);
        message1.setExpirationDate(date2);
        message1.setId(1);

        doThrow(new MessageNotFoundException()).when(messageServiceMock).updateMessage(3, message1);

        messageServiceMock.updateMessage(3, message1);
    }

    /**
     * Delete a valid message.
     * @result HTTP Status 200 Ok. Deleted correctly.
     */
    @Test
    public void deleteMessage() throws Exception {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss ");
        Date currentTime = new Date(System.currentTimeMillis());
        Date date2 =  new Date(currentTime.getTime() + (1000 * 60 * 60 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 60 * 60 * 24));

        Message message1 = new Message();
        message1.setTitle("Test");
        message1.setContent("This is a test message.");
        message1.setCreationDate(date3);
        message1.setExpirationDate(date2);
        message1.setId(1);

        mockMvc.perform(delete("/api/v1/message/1")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    /**
     * Wrong id entered.
     * @throws Exception
     */
    @Test(expected = MessageNotFoundException.class)
    public void deleteMessage_shouldThrowExceptionWhenWrongIdIsEntered() throws Exception{
        doThrow(new MessageNotFoundException()).when(messageServiceMock).deleteMessage(3);
        messageServiceMock.deleteMessage(3);
    }
}