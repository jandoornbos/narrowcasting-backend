package com.stenden.narrowcasting.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stenden.narrowcasting.configuration.TestContext;
import com.stenden.narrowcasting.configuration.WebContext;
import com.stenden.narrowcasting.model.request.AccessTokenRequest;
import com.stenden.narrowcasting.model.response.AccessTokenResponse;
import com.stenden.narrowcasting.service.AuthenticationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestContext.class, WebContext.class })
@WebAppConfiguration
public class AuthenticationControllerTest {

    private MockMvc mockMvc;
    ObjectMapper objectMapper =  new ObjectMapper();

    @Autowired
    private AuthenticationService authenticationServiceMock;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp(){
        Mockito.reset(this.authenticationServiceMock);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void getToken_shouldReturnToken() throws Exception {
        AccessTokenRequest request = new AccessTokenRequest();
        request.setUsername("jan");
        request.setPassword("1234");
        request.setClientId("1");
        request.setClientSecret("abcd");
        request.setGrantType("password");

        String jsonString = this.objectMapper.writeValueAsString(request);
        when(this.authenticationServiceMock.authenticateUser(request)).thenReturn(new AccessTokenResponse());

        this.mockMvc.perform(post("/api/v1/auth/token")
            .content(jsonString)
            .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
