package com.stenden.narrowcasting.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stenden.narrowcasting.configuration.TestContext;
import com.stenden.narrowcasting.configuration.WebContext;
import com.stenden.narrowcasting.exception.SettingNotFoundException;
import com.stenden.narrowcasting.exception.UserNotFoundException;
import com.stenden.narrowcasting.service.SettingService;
import com.stenden.narrowcasting.model.Setting;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestContext.class, WebContext.class })
@WebAppConfiguration
public class SettingControllerTest {

    private MockMvc mockMvc;
    ObjectMapper objectMapper =  new ObjectMapper();

    @Autowired
    private SettingService settingServiceMock;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        Mockito.reset(this.settingServiceMock);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    /**
     * Get valid settings.
     * @result HTTP Status 200 Ok. Retrieved correctly.
     */
    @Test
    public void getSettings() throws Exception {
        Setting setting1 = new Setting();
        setting1.setId(1);
        setting1.setDescription("setting 1.");
        setting1.setValue("value 1.");
        setting1.setKey("key 1.");

        when(this.settingServiceMock.getSettings()).thenReturn(Arrays.asList(setting1));

        this.mockMvc.perform(get("/api/v1/setting"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].description", is("setting 1.")))
                .andExpect(jsonPath("$[0].value", is("value 1.")))
                .andExpect(jsonPath("$[0].key", is("key 1.")));
    }

    /**
     * Get a valid setting.
     * @result HTTP Status 200 Ok. Retrieved correctly.
     */
    @Test
    public void getSingleSetting() throws Exception {
        Setting setting1 = new Setting();
        setting1.setId(1);
        setting1.setDescription("setting 1.");
        setting1.setValue("value 1.");
        setting1.setKey("key 1.");

        when(this.settingServiceMock.getSettingForKey("1")).thenReturn(setting1);

        this.mockMvc.perform(get("/api/v1/setting/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.description", is("setting 1.")))
                .andExpect(jsonPath("$.value", is("value 1.")))
                .andExpect(jsonPath("$.key", is("key 1.")));
    }

    /**
     * Wrong key entered.
     * @throws Exception
     */
    @Test(expected = SettingNotFoundException.class)
    public void getSingleSetting_shouldThrowExceptionWhenWrongKeyIsEntered() throws Exception{
        doThrow(new SettingNotFoundException()).when(settingServiceMock).getSettingForKey("testtest");

        settingServiceMock.getSettingForKey("testtest");
    }

    /**
     * Update setting.
     * @result HTTP Status 201 Ok. Updated correctly.
     */
    @Test
    public void updateSetting() throws Exception {
        Setting setting1 = new Setting();
        setting1.setId(1);
        setting1.setDescription("setting 1.");
        setting1.setValue("value 1.");
        setting1.setKey("key 1.");

        String json = objectMapper.writeValueAsString(setting1);
        when(this.settingServiceMock.updateSettingForKey("1", setting1)).thenReturn(setting1);

        mockMvc.perform(put("/api/v1/setting/1")
                .contentType(APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());
    }

    /**
     * Wrong key entered.
     * @throws Exception
     */
    @Test(expected = SettingNotFoundException.class)
    public void updateSetting_shouldThrowExceptionWhenWrongKeyIsEntered() throws Exception{
        Setting setting1 = new Setting();
        setting1.setId(1);
        setting1.setDescription("setting 1.");
        setting1.setValue("value 1.");
        setting1.setKey("key 1.");

        doThrow(new SettingNotFoundException()).when(settingServiceMock).updateSettingForKey("testtest", setting1);

        settingServiceMock.updateSettingForKey("testtest", setting1);
    }
}