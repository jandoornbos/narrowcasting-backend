package com.stenden.narrowcasting.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stenden.narrowcasting.configuration.TestContext;
import com.stenden.narrowcasting.configuration.WebContext;
import com.stenden.narrowcasting.exception.TeacherNotFoundException;
import com.stenden.narrowcasting.model.Teacher;
import com.stenden.narrowcasting.service.TeacherService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestContext.class, WebContext.class })
@WebAppConfiguration
public class TeacherControllerTest {

    private MockMvc mockMvc;
    ObjectMapper objectMapper =  new ObjectMapper();

    @Autowired
    private TeacherService teacherServiceMock;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        Mockito.reset(this.teacherServiceMock);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    /**
     * Get valid teachers.
     * @result HTTP Status 200 Ok. Retrieved correctly.
     */
    @Test
    public void getTeachers() throws Exception {
        Teacher teacher1 = new Teacher();
        teacher1.setId(1);
        teacher1.setFullName("test name teacher 1.");
        teacher1.setAvailability(true);

        when(this.teacherServiceMock.getAllTeachers()).thenReturn(Arrays.asList(teacher1));

        this.mockMvc.perform(get("/api/v1/teacher"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].fullName", is("test name teacher 1.")))
                .andExpect(jsonPath("$[0].availability", is(true)));
    }

    /**
     * Get a valid teacher.
     * @result HTTP Status 200 Ok. Retrieved correctly.
     */
    @Test
    public void getTeacher() throws Exception {
        Teacher teacher2 = new Teacher();
        teacher2.setId(2);
        teacher2.setFullName("test name teacher 2.");
        teacher2.setAvailability(true);

        when(this.teacherServiceMock.getSingleTeacher(2)).thenReturn(teacher2);

        this.mockMvc.perform(get("/api/v1/teacher/2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.fullName", is("test name teacher 2.")))
                .andExpect(jsonPath("$.availability", is(true)));
    }

    /**
     * Create a valid teacher.
     * @result HTTP Status 201 Ok. Created correctly.
     */
    @Test
    public void saveTeacher() throws Exception {
        Teacher teacher3 = new Teacher();
        teacher3.setId(3);
        teacher3.setFullName("test name teacher 3.");
        teacher3.setAvailability(true);

        String json = objectMapper.writeValueAsString(teacher3);
        when(this.teacherServiceMock.saveTeacher(teacher3)).thenReturn(teacher3);

        mockMvc.perform(post("/api/v1/teacher")
                .contentType(APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
    }

    /**
     * Update a valid teacher.
     * @result HTTP Status 200 Ok. Updated correctly.
     */
    @Test
    public void updateTeacher() throws Exception {
        Teacher teacher4 = new Teacher();
        teacher4.setId(4);
        teacher4.setFullName("test name teacher 4.");
        teacher4.setAvailability(true);

        String json = objectMapper.writeValueAsString(teacher4);
        when(this.teacherServiceMock.updateTeacher(4, teacher4)).thenReturn(teacher4);

        mockMvc.perform(patch("/api/v1/teacher/4")
                .contentType(APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());
    }

    /**
     * Throws an exception if no teacher is found while trying to delete an teacher
     * @throws Exception
     */
    @Test(expected = TeacherNotFoundException.class)
    public void updateTeacher_shouldThrowExceptionWhenNoTeacherIsFound() throws Exception{
        Teacher teacher4 = new Teacher();
        teacher4.setId(4);
        teacher4.setFullName("test name teacher 4.");
        teacher4.setAvailability(true);

        doThrow(new TeacherNotFoundException()).when(teacherServiceMock).updateTeacher(2, teacher4);

        teacherServiceMock.updateTeacher(2, teacher4);
    }

    /**
     * Delete a valid teacher.
     * @result HTTP Status 200 Ok. Deleted correctly.
     */
    @Test
    public void deleteTeacher() throws Exception {
        Teacher teacher5 = new Teacher();
        teacher5.setId(5);
        teacher5.setFullName("test name teacher 5.");
        teacher5.setAvailability(true);

        when(this.teacherServiceMock.deleteTeacher(5)).thenReturn(teacher5);

        this.mockMvc.perform(delete("/api/v1/teacher/5"))
                .andExpect(status().isOk());
    }

    /**
     * Throws an exception if no teacher is found while trying to delete an teacher
     * @throws Exception
     */
    @Test(expected = TeacherNotFoundException.class)
    public void deleteTeacher_shouldThrowExceptionWhenNoTeacherIsFound() throws Exception{
        doThrow(new TeacherNotFoundException()).when(teacherServiceMock).deleteTeacher(2);

        teacherServiceMock.deleteTeacher(2);
    }




}