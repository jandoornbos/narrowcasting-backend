package com.stenden.narrowcasting.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stenden.narrowcasting.exception.DateNotValidException;
import com.stenden.narrowcasting.exception.EventNotFoundException;
import com.stenden.narrowcasting.configuration.TestContext;
import com.stenden.narrowcasting.configuration.WebContext;
import com.stenden.narrowcasting.exception.StatusNotFoundException;
import com.stenden.narrowcasting.model.Event;
import com.stenden.narrowcasting.service.EventService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestContext.class, WebContext.class })
@WebAppConfiguration
public class EventControllerTest {

    private MockMvc mockMvc;
    ObjectMapper objectMapper =  new ObjectMapper();

    @Autowired
    private EventService eventServiceMock;

    @Autowired
    private WebApplicationContext webApplicationContext;

    /**
     * Runs the following code before each test
     */
    @Before
    public void setUp(){
        Mockito.reset(this.eventServiceMock);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    /**
     * Tests if the code returns all events
     * @throws Exception
     */
    @Test
    public void getEvents_shouldReturnAllEvents() throws Exception {
        Event event1 = new Event();
        event1.setCreationDate(new Date());
        event1.setEventDate(new Date());
        event1.setLocation("location1");
        event1.setTitle("title1");
        event1.setContent("content1");
        event1.setId(1);

        Event event2 = new Event();
        event2.setCreationDate(new Date());
        event2.setEventDate(new Date());
        event2.setLocation("location2");
        event2.setTitle("title2");
        event2.setContent("content2");
        event2.setId(2);


        when(this.eventServiceMock.getEvents()).thenReturn(Arrays.asList(event1, event2));

        this.mockMvc.perform(get("/api/v1/event"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[0].location", is("location1")))
                .andExpect(jsonPath("$[1].location", is("location2")))
                .andExpect(jsonPath("$[0].title", is("title1")))
                .andExpect(jsonPath("$[1].title", is("title2")));
    }

    /**
     * Tests whether de code returns all events with the correct status
     * @throws Exception
     */
    @Test
    public void getEvents_shouldReturnAllEventsWithTheRightStatus() throws Exception {

        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss ");
        Date currentTime = new Date();

        Date date2 =  new Date(currentTime.getTime() + (1000 * 3600 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 3600 * 24));
        Date date4 =  new Date(currentTime.getTime() - (1000 * 3600 * 24 * 2));

        Event event1 = new Event();
        event1.setCreationDate(date3);
        event1.setEventDate(date2);
        event1.setLocation("location1");
        event1.setTitle("title1");
        event1.setContent("content1");
        event1.setId(1);

        Event event2 = new Event();
        event2.setCreationDate(date4);
        event2.setEventDate(date3);
        event2.setLocation("location2");
        event2.setTitle("title2");
        event2.setContent("content2");
        event2.setId(2);

        when(this.eventServiceMock.getEventsByStatus(1)).thenReturn(Arrays.asList(event1));
        when(this.eventServiceMock.getEventsByStatus(0)).thenReturn(Arrays.asList(event2));

        this.mockMvc.perform(get("/api/v1/event?status=1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$[0].creationDate", is(date3.getTime())))
                .andExpect(jsonPath("$[0].eventDate", is(date2.getTime())));

        this.mockMvc.perform(get("/api/v1/event?status=0"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$[0].creationDate", is(date4.getTime())))
                .andExpect(jsonPath("$[0].eventDate", is(date3.getTime())));
    }

    /**
     * Tests if the code returns a single event with the correct id
     * @throws Exception
     */
    @Test
    public void getEvent_shouldReturnSingleEvent() throws Exception{
        Event event1 = new Event();
        event1.setCreationDate(new Date());
        event1.setEventDate(new Date());
        event1.setLocation("location1");
        event1.setTitle("title1");
        event1.setContent("content1");
        event1.setId(1);

        Event event2 = new Event();
        event2.setCreationDate(new Date());
        event2.setEventDate(new Date());
        event2.setLocation("location2");
        event2.setTitle("title2");
        event2.setContent("content2");
        event2.setId(2);

        when(this.eventServiceMock.getSingleEvent(2)).thenReturn(event2);

        this.mockMvc.perform(get("/api/v1/event/2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(2)));
    }

    /**
     * Tests whether the code throws an exception if no event matches the enterd id when looking for an event
     * @throws Exception
     */
    @Test(expected = EventNotFoundException.class)
    public void getEvent_shouldThrowExceptionWhenNoEventIsFound() throws Exception{
        doThrow(new EventNotFoundException()).when(eventServiceMock).getSingleEvent(2);

        eventServiceMock.getSingleEvent(2);
    }

    /**
     * Tests whether the code throws an exception if no status matches the entered status
     * @throws Exception
     */
    @Test(expected = StatusNotFoundException.class)
    public void getEvent_shouldThrowExceptionWhenWrongStatusNrIsEntered() throws Exception{
        doThrow(new StatusNotFoundException()).when(eventServiceMock).getEventsByStatus(2);

        eventServiceMock.getEventsByStatus(2);
    }

    /**
     * Tests whether the saved object contains data in all fields after saving an event
     * @throws Exception
     */
    @Test
    public void saveEvent_shouldSaveEventCorrectlyAndContainDataInAllFields() throws Exception
    {
        Date currentTime = new Date();
        Date date2 =  new Date(currentTime.getTime() + (1000 * 3600 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 3600 * 24));

        Event event1 = new Event();
        event1.setCreationDate(date3);
        event1.setEventDate(date2);
        event1.setLocation("location1");
        event1.setTitle("title1");
        event1.setContent("content1");
        event1.setId(1);

        String json = objectMapper.writeValueAsString(event1);

        when(this.eventServiceMock.saveEvent(any(Event.class))).thenReturn(event1);

        this.mockMvc.perform(post("/api/v1/event")
            .contentType(MediaType.APPLICATION_JSON)
            .content(json))

            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.creationDate", is(date3.getTime())))
            .andExpect(jsonPath("$.eventDate", is(date2.getTime())))
            .andExpect(jsonPath("$.location", is("location1")))
            .andExpect(jsonPath("$.title", is("title1")))
            .andExpect(jsonPath("$.content", is("content1")))
            .andExpect(jsonPath("$.id", is(1)));
    }

    /**
     * Tests whether saveEvent throws an exception if the eventdate is before the creation date
     * @throws Exception
     */
    @Test(expected = DateNotValidException.class)
    public void saveEvent_shouldNotHaveCreationDateHigherThanEventDateAndShouldThrowException() throws Exception{
        Date currentTime = new Date();
        Date date2 =  new Date(currentTime.getTime() + (1000 * 3600 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 3600 * 24));

        Event event1 = new Event();
        event1.setCreationDate(date2);
        event1.setEventDate(date3);
        event1.setLocation("location1");
        event1.setTitle("title1");
        event1.setContent("content1");
        event1.setId(1);

        doThrow(new DateNotValidException()).when(eventServiceMock).saveEvent(event1);

        eventServiceMock.saveEvent(event1);
    }

    /**
     * Tests whether the code throws an exception if no id matches the entered one
     * @throws Exception
     */
    @Test(expected = EventNotFoundException.class)
    public void updateEvent_shouldThrowExceptionWhenWrongIdIsEntered() throws Exception
    {
        Date currentTime = new Date();
        Date date2 =  new Date(currentTime.getTime() + (1000 * 3600 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 3600 * 24));

        Event event1_updated = new Event();
        event1_updated.setCreationDate(date3);
        event1_updated.setEventDate(date2);
        event1_updated.setLocation("location2");
        event1_updated.setTitle("title1");
        event1_updated.setContent("content2");
        event1_updated.setId(1);

        doThrow(new EventNotFoundException()).when(eventServiceMock).updateEvent(2,event1_updated);

        eventServiceMock.updateEvent(2, event1_updated);
    }

    /**
     * Tests weather an event is properly updated
     * @throws Exception
     */
    @Test
    public void updateEvent_shouldProperlyUpdateEvent() throws Exception
    {
        Date currentTime = new Date();
        Date date2 =  new Date(currentTime.getTime() + (1000 * 3600 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 3600 * 24));

        Event event1 = new Event();
        event1.setCreationDate(date3);
        event1.setEventDate(date2);
        event1.setLocation("location1");
        event1.setTitle("title1");
        event1.setContent("content1");
        event1.setId(1);

        String json = objectMapper.writeValueAsString(event1);

        when(this.eventServiceMock.updateEvent(1, event1)).thenReturn(event1);

        mockMvc.perform(put("/api/v1/event/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(json))
            .andExpect(status().isOk());
    }

    /**
     * Tests whether the event in the database contains data in all fields after updating
     * @throws Exception
     */
    @Test
    public void updateEvent_shouldContainDataInAllFields() throws Exception{
        Date currentTime = new Date();
        Date date2 =  new Date(currentTime.getTime() + (1000 * 3600 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 3600 * 24));

        Event event1 = new Event();
        event1.setCreationDate(date3);
        event1.setEventDate(date2);
        event1.setLocation("location1");
        event1.setTitle("title1");
        event1.setContent("content1");
        event1.setId(1);

        Event event1_updated = new Event();
        event1_updated.setCreationDate(date3);
        event1_updated.setEventDate(date2);
        event1_updated.setLocation("location2");
        event1_updated.setTitle("title1");
        event1_updated.setContent("content2");
        event1_updated.setId(1);

        when(this.eventServiceMock.updateEvent(1, event1_updated)).thenReturn(event1_updated);
        when(this.eventServiceMock.getSingleEvent(1)).thenReturn(event1_updated);

        eventServiceMock.saveEvent(event1);
        eventServiceMock.updateEvent(1, event1_updated );

        mockMvc.perform(get("/api/v1/event/1"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.eventDate", is(date2.getTime())))
            .andExpect(jsonPath("$.creationDate", is(date3.getTime())))
            .andExpect(jsonPath("$.location", is("location2")))
            .andExpect(jsonPath("$.title", is("title1")))
            .andExpect(jsonPath("$.content", is("content2")))
            .andExpect(jsonPath("$.id", is(1)));
    }

    /**
     * tests wether the event is truely deleted after deleting
     * @throws Exception
     */
    @Test
    public void deleteEvent_shouldProperlyDeleteEvent() throws Exception {
        Date currentTime = new Date();
        Date date2 =  new Date(currentTime.getTime() + (1000 * 3600 * 24 * 2));
        Date date3 =  new Date(currentTime.getTime() - (1000 * 3600 * 24));

        Event event1 = new Event();
        event1.setCreationDate(date3);
        event1.setEventDate(date2);
        event1.setLocation("location1");
        event1.setTitle("title1");
        event1.setContent("content1");
        event1.setId(1);

        eventServiceMock.saveEvent(event1);

        mockMvc.perform(delete("/api/v1/event/1"))
            .andExpect(status().isOk());
    }

    /**
     * Tests whether an exception is thrown in the deleteEvent Class if an incorrect id is entered
     * @throws Exception
     */
    @Test(expected = EventNotFoundException.class)
    public void deleteEvent_shouldThrowExceptionWhenWrongIdIsEntered() throws Exception
    {
        doThrow(new EventNotFoundException()).when(eventServiceMock).deleteEvent(2);

        eventServiceMock.deleteEvent(2);
    }
}
