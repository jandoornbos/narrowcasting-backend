package com.stenden.narrowcasting.repository;

import com.stenden.narrowcasting.model.oauth.RefreshToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;

@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Integer> {
    RefreshToken findByToken(String token);

    @Modifying
    @Transactional
    @Query("DELETE FROM RefreshToken a WHERE a.expiresAt < :date")
    void removeExpiredRefreshTokens(@Param("date") Timestamp timestamp);
}
