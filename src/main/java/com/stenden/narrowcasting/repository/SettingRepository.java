package com.stenden.narrowcasting.repository;

import com.stenden.narrowcasting.model.Setting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SettingRepository extends JpaRepository<Setting, Integer> {
    Setting findByKey(String key);
}
