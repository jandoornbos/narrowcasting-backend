package com.stenden.narrowcasting.repository;

import com.stenden.narrowcasting.model.oauth.AccessToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;

@Repository
public interface AccessTokenRepository extends JpaRepository<AccessToken, Integer> {
    AccessToken findByToken(String token);

    @Modifying
    @Transactional
    @Query("DELETE FROM AccessToken a WHERE a.expiresAt < :date")
    void removeExpiredAccessTokens(@Param("date") Timestamp timestamp);
}
