package com.stenden.narrowcasting.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "teacher")
public class Teacher extends BaseModel {

    @NotEmpty(message = "Full name can not be empty.")
    @Column(name = "full_name")
    private String fullName;

    @Column(name= "availability")
    private Boolean availability;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Boolean getAvailability() {
        return availability;
    }

    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }
}
