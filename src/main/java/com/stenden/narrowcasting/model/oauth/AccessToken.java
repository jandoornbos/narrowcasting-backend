package com.stenden.narrowcasting.model.oauth;

import com.stenden.narrowcasting.model.BaseModel;
import com.stenden.narrowcasting.model.User;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "access_token")
public class AccessToken extends BaseModel {

    @Column(name = "token")
    private String token;

    @Column(name = "expires_at")
    private Timestamp expiresAt;

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Timestamp getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Timestamp expiresAt) {
        this.expiresAt = expiresAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
