package com.stenden.narrowcasting.advice;

import com.stenden.narrowcasting.model.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class ExceptionControllerAdvice {

    /**
     * Catch all the exceptions and format them to a readable JSON format.
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        // Catch our own api exceptions
        if (ex instanceof ApiException) {
            ApiException exception = (ApiException) ex;
            error.setErrorCode(exception.getHttpStatus().value());
            error.setErrorMessage(exception.getErrorMessage());
            return new ResponseEntity<ErrorResponse>(error, exception.getHttpStatus());
        }

        // Catch validation exceptions
        if (ex instanceof ConstraintViolationException) {
            ConstraintViolationException exception = (ConstraintViolationException) ex;
            error.setErrorCode(HttpStatus.BAD_REQUEST.value());
            error.setErrorMessage(exception.getConstraintViolations().iterator().next().getMessageTemplate());
            return new ResponseEntity<ErrorResponse>(error, HttpStatus.BAD_REQUEST);
        }

        // Catch empty body exceptions
        if (ex instanceof HttpMessageNotReadableException) {
            error.setErrorCode(HttpStatus.BAD_REQUEST.value());
            error.setErrorMessage("Request body is empty.");
            return new ResponseEntity<ErrorResponse>(error, HttpStatus.BAD_REQUEST);
        }

        // Catch 404 exceptions
        if (ex instanceof NoHandlerFoundException) {
            error.setErrorCode(HttpStatus.NOT_FOUND.value());
            error.setErrorMessage("Endpoint not found.");
            return new ResponseEntity<ErrorResponse>(error, HttpStatus.NOT_FOUND);
        }

        // All other exceptions
        int statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
        error.setErrorCode(statusCode);
        error.setErrorMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
