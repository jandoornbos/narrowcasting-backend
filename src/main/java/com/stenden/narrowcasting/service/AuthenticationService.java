package com.stenden.narrowcasting.service;

import com.stenden.narrowcasting.model.oauth.AccessToken;
import com.stenden.narrowcasting.model.request.AccessTokenRequest;
import com.stenden.narrowcasting.model.response.AccessTokenResponse;

public interface AuthenticationService {
    AccessToken getAccessToken(String token);
    AccessTokenResponse authenticateUser(AccessTokenRequest request);
    AccessTokenResponse refreshToken(AccessTokenRequest request);
    void removeExpiredAccessTokens();
    void removeExpiredRefreshTokens();
}
