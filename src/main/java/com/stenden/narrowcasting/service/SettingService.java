package com.stenden.narrowcasting.service;

import com.stenden.narrowcasting.model.Setting;

import java.util.List;

public interface SettingService {
    List<Setting> getSettings();
    Setting getSettingForKey(String key);
    Setting updateSettingForKey(String key, Setting setting);
}
