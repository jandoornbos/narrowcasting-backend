package com.stenden.narrowcasting.service;

import com.stenden.narrowcasting.model.User;

import java.util.List;

public interface UserService {
    List<User> getUsers();
    User updateUser(int userId, User user);
    User getUserByUsername(String username);
    User getSingleUser(int userId);
    User createUser(User user);
    void deleteUser(int userId);
}
