package com.stenden.narrowcasting.service;

import com.stenden.narrowcasting.model.Message;

import java.util.List;

public interface MessageService {
    List<Message> getMessages();
    List<Message> getMessagesByStatus(int status);
    Message getSingleMessage(int id);
    Message saveMessage(Message message);
    Message updateMessage(int messageId, Message message);
    void deleteMessage(int messageId);
}
