package com.stenden.narrowcasting.service;

import com.stenden.narrowcasting.model.Event;
import org.springframework.stereotype.Component;

import java.util.List;

public interface EventService {
    List<Event> getEvents();
    List<Event> getEventsByStatus(int status);
    Event getSingleEvent(int eventId);
    Event saveEvent(Event event);
    Event updateEvent(int eventId, Event event);
    void deleteEvent(int eventId);
}
