package com.stenden.narrowcasting.service.impl;

import com.stenden.narrowcasting.advice.ApiException;
import com.stenden.narrowcasting.exception.TeacherNotFoundException;
import com.stenden.narrowcasting.model.Teacher;
import com.stenden.narrowcasting.repository.TeacherRepository;
import com.stenden.narrowcasting.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherRepository teacherRepository;

    /**
     * Get all teachers.
     * @return A list of teachers.
     */
    public List<Teacher> getAllTeachers() {
        return this.teacherRepository.findAll();
    }

    /**
     * Get a single teacher by id.
     * @param teacherId The teacher id.
     * @return A teacher.
     * @throws TeacherNotFoundException The teacher could not be found.
     */
    public Teacher getSingleTeacher(int teacherId) {
        return this.teacherRepository.findOne(teacherId);
    }

    /**
     * Create a new teacher.
     * @param teacher The teacher to create.
     * @return The created teacher.
     */
    public Teacher saveTeacher(Teacher teacher) {
        return this.teacherRepository.saveAndFlush(teacher);
    }

    /**
     * Update a teacher.
     * @param teacherId The teacher id.
     * @param teacher The new teacher object.
     * @return The updated teacher object.
     * @throws TeacherNotFoundException The teacher could not be found.
     */
    public Teacher updateTeacher(int teacherId, Teacher teacher) {
        Teacher oldTeacher = this.teacherRepository.findOne(teacherId);
        if (oldTeacher == null) {
            throw new TeacherNotFoundException();
        }
        if (teacher.getFullName() != null) {
            oldTeacher.setFullName(teacher.getFullName());
        }
        if (teacher.getAvailability() == true) {
            oldTeacher.setAvailability(true);
        } else if(teacher.getAvailability() == false) {
            oldTeacher.setAvailability(false);
        }
        return this.teacherRepository.save(oldTeacher);
    }

    /**
     * Delete a teacher.
     * @param teacherId The id of the teacher.
     * @throws TeacherNotFoundException The teacher could not be found.
     */
    public Teacher deleteTeacher(int teacherId) {
        Teacher oldTeacher = this.teacherRepository.findOne(teacherId);
        if(oldTeacher == null) {
            throw new TeacherNotFoundException();
        } else {
            this.teacherRepository.delete(teacherId);
            return oldTeacher;
        }
    }

    /**
     * Reset the teachers availability.
     */
    public void resetTeacherAvailability() {
        this.teacherRepository.resetTeachersAvailability();
    }
}
