package com.stenden.narrowcasting.service.impl;

import com.stenden.narrowcasting.exception.DateNotValidException;
import com.stenden.narrowcasting.exception.EventNotFoundException;
import com.stenden.narrowcasting.exception.StatusNotFoundException;
import com.stenden.narrowcasting.model.Event;
import com.stenden.narrowcasting.repository.EventRepository;
import com.stenden.narrowcasting.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Martijn on 10-1-2017.
 */

@Component
public class EventServiceImpl implements EventService {

    @Autowired
    private EventRepository eventRepository;

    /**
     * Retrieve all the events from the repository
     * @return List of all events
     */
    public List<Event> getEvents() {
        return this.eventRepository.findAll();
    }

    /**
     * Retrieve all the events from the repository with a current status
     * @param status status of the event (1 = not passed yet, 0 = already passed)
     * @return List of wanted events
     */
    public List<Event> getEventsByStatus(int status){
        List<Event> fullEventList = this.eventRepository.findAll();
        List<Event> sortedEventListActive = new ArrayList<Event>();
        List<Event> sortedEventListPassed = new ArrayList<Event>();

        Date currentDate = new Date();

        // Check if the given status is within the allowed range
        if(status != 1 && status != 0)
            throw new StatusNotFoundException();

        //Loop through al the events looking for the wanted statusses
        for (Event event: fullEventList) {
            if(event.getEventDate() != null){

                Calendar currCall = Calendar.getInstance();
                Calendar eventCall = Calendar.getInstance();
                currCall.setTime(currentDate);
                eventCall.setTime(event.getEventDate());

                if((currentDate.getTime() / (1000 * 3600 * 24)) <= (event.getEventDate().getTime() / (1000 * 3600 * 24))){
                    sortedEventListActive.add(event);
                }
                else{
                    sortedEventListPassed.add(event);
                }
            }
            else{
                sortedEventListPassed.add(event);
            }
        }
        if(status == 1) {
            return sortedEventListActive;
        }
        else {
            return sortedEventListPassed;
        }
    }

    /**
     * Save a new event to the repository
     * @param event The event to save to the repository
     * @return The saved event
     */
    public Event saveEvent(Event event) {
        if(event.getEventDate().before(event.getCreationDate()))
            throw new DateNotValidException();
        return this.eventRepository.saveAndFlush(event);
    }

    /**
     * Updates an event in the repository to an event with new values
     * @param eventId The id of the event to update
     * @param event The new content of the event
     * @return The updated event
     */
    public Event updateEvent(int eventId, Event event) {
        //Retrieve the old event from the repository
        Event oldEvent = this.eventRepository.findOne(eventId);
        if(oldEvent == null)
            throw new EventNotFoundException();

        //Update the old event with the new values
        if(event.getContent() != null)
            oldEvent.setContent(event.getContent());
        if(event.getEventDate() != null)
            oldEvent.setEventDate(event.getEventDate());
        if(event.getTitle() != null)
            oldEvent.setTitle(event.getTitle());
        if(event.getLocation() != null)
            oldEvent.setLocation(event.getLocation());

        //Save the updated event to the repositories
        return this.eventRepository.save(oldEvent);
    }

    /**
     * Deletes an event out of the database
     * @param eventId The id of the event to be deleled
     */
    public void deleteEvent(int eventId){
        //Check if the event is available in the repository
        Event eventToBeDeleted = this.eventRepository.findOne(eventId);
        if(eventToBeDeleted == null)
            throw new EventNotFoundException();

        this.eventRepository.delete(eventId);
    }

    /**
     * Retrieve a single event from the database
     * @param eventId the id of the event to retreive
     * @return the wanted event
     */
    public Event getSingleEvent(int eventId) {
        Event event = this.eventRepository.findOne(eventId);
        if (event == null) {
            throw new EventNotFoundException();
        } else {
            return event;
        }
    }





}
