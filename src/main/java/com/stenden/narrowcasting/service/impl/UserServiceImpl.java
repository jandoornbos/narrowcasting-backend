package com.stenden.narrowcasting.service.impl;

import com.stenden.narrowcasting.advice.ApiException;
import com.stenden.narrowcasting.exception.UserNotFoundException;
import com.stenden.narrowcasting.model.User;
import com.stenden.narrowcasting.repository.UserRepository;
import com.stenden.narrowcasting.service.UserService;
import com.stenden.narrowcasting.util.Password;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    /**
     * Get all users.
     * @return A list of User.
     */
    public List<User> getUsers() {
        return this.userRepository.findAll();
    }

    public User getUserByUsername(String username) {
        User user = this.userRepository.findByUsername(username);
        if (user == null) {
            throw new UserNotFoundException();
        } else {
            return user;
        }
    }

    /**
     * Get a single user by id.
     * @param userId The user id.
     * @return A user.
     * @throws UserNotFoundException The user could not be found.
     */
    public User getSingleUser(int userId) {
        User user = this.userRepository.findOne(userId);
        if (user == null) {
            throw new UserNotFoundException();
        } else {
            return user;
        }
    }

    /**
     * Update a user.
     * @param userId The user id.
     * @param user The new user object.
     * @return The updated user object.
     * @throws UserNotFoundException The user could not be found.
     */
    public User updateUser(int userId, User user) {
        User oldUser = this.userRepository.findOne(userId);
        if (oldUser == null) {
            throw new UserNotFoundException();
        }

        if (user.getPassword() != null) {
            oldUser.setPassword(Password.hashPassword(user.getPassword()));
        }
        if (user.getUsername() != null) {
            oldUser.setUsername(user.getUsername());
        }
        if (user.getScreenname() != null) {
            oldUser.setScreenname(user.getScreenname());
        }

        return this.userRepository.save(oldUser);
    }

    /**
     * Create a new user.
     * @param user The user to create.
     * @return The created user.
     */
    public User createUser(User user) {
        // Hash the password before saving
        String hashed = Password.hashPassword(user.getPassword());
        user.setPassword(hashed);
        return this.userRepository.saveAndFlush(user);
    }

    /**
     * Delete a user.
     * @param userId The user id of the user.
     * @throws UserNotFoundException The user could not be found.
     */
    public void deleteUser(int userId) {
        User user = this.userRepository.findOne(userId);
        if (user == null) {
            throw new UserNotFoundException();
        } else {
            this.userRepository.delete(userId);
        }
    }
}
