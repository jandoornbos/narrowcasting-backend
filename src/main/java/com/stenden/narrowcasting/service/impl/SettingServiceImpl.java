package com.stenden.narrowcasting.service.impl;

import com.stenden.narrowcasting.exception.SettingNotFoundException;
import com.stenden.narrowcasting.model.Setting;
import com.stenden.narrowcasting.repository.SettingRepository;
import com.stenden.narrowcasting.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SettingServiceImpl implements SettingService {

    @Autowired
    SettingRepository settingRepository;

    /**
     * Get all settings.
     * @return A list of Setting.
     */
    public List<Setting> getSettings() {
        return this.settingRepository.findAll();
    }

    /**
     * Get a setting for the given key.
     * @param key The key to search for.
     * @return The setting.
     * @throws SettingNotFoundException The setting could not be found.
     */
    public Setting getSettingForKey(String key) {
        Setting setting = this.settingRepository.findByKey(key);
        if (setting == null) {
            throw new SettingNotFoundException();
        } else {
            return setting;
        }
    }

    /**
     * Update a setting for the given key.
     * @param key The setting to update.
     * @param setting The new setting object.
     * @return The setting.
     * @throws SettingNotFoundException The setting could not be found.
     */
    public Setting updateSettingForKey(String key, Setting setting) {
        Setting currentSetting = this.settingRepository.findByKey(key);
        if (setting != null) {
            currentSetting.setValue(setting.getValue());
            return this.settingRepository.save(currentSetting);
        } else {
            throw new SettingNotFoundException();
        }
    }


}
