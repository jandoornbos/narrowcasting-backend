package com.stenden.narrowcasting.service.impl;

import com.stenden.narrowcasting.advice.ApiException;
import com.stenden.narrowcasting.exception.DateNotValidException;
import com.stenden.narrowcasting.exception.MessageNotFoundException;
import com.stenden.narrowcasting.exception.StatusNotFoundException;
import com.stenden.narrowcasting.model.Message;
import com.stenden.narrowcasting.repository.MessageRepository;
import com.stenden.narrowcasting.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepository messageRepository;

    public List<Message> getMessages() {
        return this.messageRepository.findAll();
    }

    /**
     * Retrieve all the messages from the repository with a current status
     * @param status status of the message (1 = not passed yet, 0 = already passed)
     * @return List of wanted messages
     */
    public List<Message> getMessagesByStatus(int status){
        List<Message> fullMessageList = this.messageRepository.findAll();
        List<Message> sortedMessageListActive = new ArrayList<Message>();
        List<Message> sortedMessageListPassed = new ArrayList<Message>();

        Date currentDate = new Date();

        // Check if the given status is within the allowed range
        if(status != 1 && status != 0)
            throw new StatusNotFoundException();

        //Loop through al the messages looking for the wanted statusses
        for (Message message: fullMessageList) {
            if (message.getExpirationDate() != null) {
                if((currentDate.getTime() / (1000 * 3600 * 24)) <= (message.getExpirationDate().getTime() / (1000 * 3600 * 24))){
                    sortedMessageListActive.add(message);
                } else {
                    sortedMessageListPassed.add(message);
                }
            } else {
                sortedMessageListPassed.add(message);
            }
        }

        if (status == 1) {
            return sortedMessageListActive;
        } else {
            return sortedMessageListPassed;
        }
    }

    public Message saveMessage(Message message) {
        if(message.getExpirationDate().before(message.getCreationDate()))
            throw new DateNotValidException();
        return this.messageRepository.saveAndFlush(message);
    }

    public Message updateMessage(int messageId, Message message) {
        Message oldMessage = this.messageRepository.findOne(messageId);
        if (oldMessage == null) {
            throw new MessageNotFoundException();
        }
        if (message.getContent() != null) {
            oldMessage.setContent(message.getContent());
        }
        if (message.getExpirationDate() != null) {
            oldMessage.setExpirationDate(message.getExpirationDate());
        }
        if (message.getTitle() != null) {
            oldMessage.setTitle(message.getTitle());
        }
        return this.messageRepository.save(oldMessage);
    }

    /**
     * Deletes an message out of the database
     * @param messageId The id of the message to be deleted
     */
    public void deleteMessage(int messageId){
        //Check if the message is available in the repository
        Message messageToBeDeleted = this.messageRepository.findOne(messageId);
        if(messageToBeDeleted == null)
            throw new MessageNotFoundException();

        this.messageRepository.delete(messageId);
    }

    /**
     * Retrieve a single message from the database
     * @param id the id of the message to retreive
     * @return the wanted message
     */
    public Message getSingleMessage(int id) {
        Message message = this.messageRepository.findOne(id);
        if (message == null) {
            throw new MessageNotFoundException();
        } else {
            return message;
        }
    }
}
