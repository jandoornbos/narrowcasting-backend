package com.stenden.narrowcasting.service.impl;

import com.stenden.narrowcasting.advice.ApiException;
import com.stenden.narrowcasting.config.Constants;
import com.stenden.narrowcasting.model.User;
import com.stenden.narrowcasting.model.oauth.AccessToken;
import com.stenden.narrowcasting.model.oauth.RefreshToken;
import com.stenden.narrowcasting.model.request.AccessTokenRequest;
import com.stenden.narrowcasting.model.response.AccessTokenResponse;
import com.stenden.narrowcasting.repository.AccessTokenRepository;
import com.stenden.narrowcasting.repository.RefreshTokenRepository;
import com.stenden.narrowcasting.service.AuthenticationService;
import com.stenden.narrowcasting.service.UserService;
import com.stenden.narrowcasting.util.AccessTokenGenerator;
import com.stenden.narrowcasting.util.Password;
import org.omg.CORBA.DynAnyPackage.Invalid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.print.DocFlavor;
import java.sql.Timestamp;

@Component
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private UserService userService;

    /**
     * Create a new access token for an user.
     * @param user The user to create the access token for.
     * @return The access token object.
     */
    private AccessToken createAccessToken(User user) {
        AccessToken token = new AccessToken();
        token.setToken(new AccessTokenGenerator().nextSessionId());
        token.setExpiresAt(new Timestamp(System.currentTimeMillis() + Constants.ACCESS_TOKEN_EXPIRE_TIME));
        token.setUser(user);
        return this.accessTokenRepository.saveAndFlush(token);
    }

    /**
     * Create a new refresh token for an user.
     * @param user The user to create the refresh token for.
     * @return The refresh token object.
     */
    private RefreshToken createRefreshToken(User user) {
        RefreshToken token = new RefreshToken();
        token.setToken(new AccessTokenGenerator().nextSessionId());
        token.setExpiresAt(new Timestamp(System.currentTimeMillis() + Constants.REFRESH_TOKEN_EXPIRE_TIME));
        token.setUser(user);
        return this.refreshTokenRepository.saveAndFlush(token);
    }

    /**
     * Check if the OAuth client is valid.
     * @param request The request send by the user.
     * @throws InvalidClientException The given client is invalid.
     */
    private void validateClient(AccessTokenRequest request) {
        if (request.getClientId() == null || request.getClientSecret() == null) {
            throw new InvalidClientException();
        }
        if (!request.getClientId().equals(Constants.CLIENT_ID) ||
                !request.getClientSecret().equals(Constants.CLIENT_SECRET)) {
            throw new InvalidClientException();
        }
    }

    /**
     * Create a new access token response, which will be returned by the api.
     * @param token The access token to use.
     * @param rToken The refresh token to use.
     * @return The access token response.
     */
    private AccessTokenResponse createResponse(AccessToken token, RefreshToken rToken) {
        AccessTokenResponse response = new AccessTokenResponse();
        response.setAccessToken(token.getToken());
        response.setExpiresIn(Constants.ACCESS_TOKEN_EXPIRE_TIME / 1000);
        response.setTokenType("Bearer");
        response.setScope(null);
        response.setRefreshToken(rToken.getToken());
        return response;
    }

    /**
     * Get an access token from the database by the token.
     * @param token The token.
     * @return An access token object.
     */
    public AccessToken getAccessToken(String token) {
        return this.accessTokenRepository.findByToken(token);
    }

    /**
     * Try to authenticate the user.
     * @param request The request send by the user.
     * @return An access token response when possible.
     * @throws InvalidCredentialsException The user credentials are invalid.
     */
    public AccessTokenResponse authenticateUser(AccessTokenRequest request) {
        this.validateClient(request);

        String username = request.getUsername();
        User user = this.userService.getUserByUsername(username);
        if (Password.checkPassword(request.getPassword(), user.getPassword())) {
            // Authentication successful
            AccessToken token = this.createAccessToken(user);
            RefreshToken rToken = this.createRefreshToken(user);
            return this.createResponse(token, rToken);
        } else {
            throw new InvalidCredentialsException();
        }
    }

    /**
     * Refresh the token for an user.
     * @param request The request from the user.
     * @return The access token response.
     * @throws RefreshTokenExpiredException The refresh token provided has expired.
     * @throws InvalidCredentialsException The refresh token doesn't exists.
     */
    public AccessTokenResponse refreshToken(AccessTokenRequest request) {
        this.validateClient(request);

        String refreshToken = request.getRefreshToken();
        RefreshToken token = this.refreshTokenRepository.findByToken(refreshToken);
        if (token != null) {
            // Check if token is not expired
            if (System.currentTimeMillis() < token.getExpiresAt().getTime()) {
                AccessToken aToken = this.createAccessToken(token.getUser());
                RefreshToken rToken = this.createRefreshToken(token.getUser());
                this.refreshTokenRepository.delete(token);
                return this.createResponse(aToken, rToken);
            } else {
                throw new RefreshTokenExpiredException();
            }
        } else {
            throw new InvalidCredentialsException();
        }
    }

    /**
     * Remove all the expired access tokens from the database.
     */
    public void removeExpiredAccessTokens() {
        this.accessTokenRepository.removeExpiredAccessTokens(new Timestamp(System.currentTimeMillis()));
    }

    /**
     * Remove all the expired refresh tokens from the database.
     */
    public void removeExpiredRefreshTokens() {
        this.refreshTokenRepository.removeExpiredRefreshTokens(new Timestamp(System.currentTimeMillis()));
    }

    private class InvalidCredentialsException extends ApiException {
        private InvalidCredentialsException() {
            super(HttpStatus.BAD_REQUEST, "Invalid credentials.");
        }
    }

    private class InvalidClientException extends ApiException {
        private InvalidClientException() {
            super(HttpStatus.BAD_REQUEST, "Invalid client.");
        }
    }

    private class RefreshTokenExpiredException extends ApiException {
        private RefreshTokenExpiredException() {
            super(HttpStatus.BAD_REQUEST, "Refresh token is expired.");
        }
    }

}
