package com.stenden.narrowcasting.service;


import com.stenden.narrowcasting.model.Teacher;

import java.util.List;

public interface TeacherService {
    List<Teacher> getAllTeachers();
    Teacher getSingleTeacher(int teacherId);
    Teacher saveTeacher(Teacher teacher);
    Teacher updateTeacher(int teacherId, Teacher teacher);
    Teacher deleteTeacher(int teacherId);
    void resetTeacherAvailability();
}
