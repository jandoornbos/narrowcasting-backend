package com.stenden.narrowcasting.util;

import java.math.BigInteger;
import java.security.SecureRandom;

public final class AccessTokenGenerator {

    private SecureRandom random = new SecureRandom();

    /**
     * Create a new random token.
     * @return The created token.
     */
    public String nextSessionId() {
        return new BigInteger(130, random).toString(32);
    }

}
