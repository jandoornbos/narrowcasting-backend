package com.stenden.narrowcasting.config;

public class Constants {
    public static final String CLIENT_ID = "1";
    public static final String CLIENT_SECRET = "abcd";

    public static final long ACCESS_TOKEN_EXPIRE_TIME = 3600 * 1000;
    public static final long REFRESH_TOKEN_EXPIRE_TIME = 3600 * 1000 * 24;
}
