package com.stenden.narrowcasting.interceptor;

import com.stenden.narrowcasting.advice.ApiException;
import com.stenden.narrowcasting.annotation.SecuredEndpoint;
import com.stenden.narrowcasting.model.oauth.AccessToken;
import com.stenden.narrowcasting.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    AuthenticationService authenticationService;

    /**
     * Intercepts all requests, and checks if the endpoint needs authentication.
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // Get the HandlerMethod
        if (handler instanceof HandlerMethod) {
            HandlerMethod method = (HandlerMethod) handler;
            Method javaMethod = method.getMethod();
            // Check if endpoint is secured
            if (javaMethod.isAnnotationPresent(SecuredEndpoint.class)) {
                // Get the authorization header
                String header = request.getHeader("Authorization");
                AccessToken token = this.authenticationService.getAccessToken(header);
                if (token != null) {
                    // Token found, check if it's not expired yet
                    if (System.currentTimeMillis() < token.getExpiresAt().getTime()) {
                        // Add the current user object to the request, so we can use
                        // this in the controller.
                        request.setAttribute("current-user", token.getUser());
                        return super.preHandle(request, response, handler);
                    } else {
                        throw new AccessTokenExpiredException();
                    }
                } else {
                    throw new AccessDeniedException();
                }
            } else {
                return super.preHandle(request, response, handler);
            }
        }
        // Every everything fails, just continue
        return super.preHandle(request, response, handler);
    }

    private class AccessDeniedException extends ApiException {
        private AccessDeniedException() {
            super(HttpStatus.UNAUTHORIZED, "You need OAuth authentication.");
        }
    }

    private class AccessTokenExpiredException extends ApiException {
        private AccessTokenExpiredException() {
            super(HttpStatus.UNAUTHORIZED, "The accesstoken provided has expired.");
        }
    }

}
