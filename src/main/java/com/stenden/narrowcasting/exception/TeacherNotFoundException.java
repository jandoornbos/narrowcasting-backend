package com.stenden.narrowcasting.exception;

import com.stenden.narrowcasting.advice.ApiException;
import org.springframework.http.HttpStatus;

public class TeacherNotFoundException extends ApiException {
    public TeacherNotFoundException() {
        super(HttpStatus.NOT_FOUND, "Teacher not found.");
    }
}