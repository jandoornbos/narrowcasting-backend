package com.stenden.narrowcasting.exception;

import com.stenden.narrowcasting.advice.ApiException;
import org.springframework.http.HttpStatus;


public class EventNotFoundException extends ApiException{
        public EventNotFoundException() {
            super(HttpStatus.NOT_FOUND, "Event not found.");
        }



}
