package com.stenden.narrowcasting.exception;

import com.stenden.narrowcasting.advice.ApiException;
import org.springframework.http.HttpStatus;


public class SettingNotFoundException extends ApiException {
    public SettingNotFoundException() {
        super(HttpStatus.NOT_FOUND, "Setting not found.");
    }
}
