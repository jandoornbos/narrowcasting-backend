package com.stenden.narrowcasting.exception;

import com.stenden.narrowcasting.advice.ApiException;
import org.springframework.http.HttpStatus;


public class StatusNotFoundException extends ApiException {
    public StatusNotFoundException() {
        super(HttpStatus.NOT_FOUND, "Status not found.");
    }
}