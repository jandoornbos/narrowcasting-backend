package com.stenden.narrowcasting.exception;

import com.stenden.narrowcasting.advice.ApiException;

import static org.springframework.http.HttpStatus.FORBIDDEN;


public class DateNotValidException extends ApiException {

        public DateNotValidException(){ super(FORBIDDEN, "Date not valid");}
}
