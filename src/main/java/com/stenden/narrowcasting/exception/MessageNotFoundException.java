package com.stenden.narrowcasting.exception;

import com.stenden.narrowcasting.advice.ApiException;
import org.springframework.http.HttpStatus;


public class MessageNotFoundException extends ApiException {
    public MessageNotFoundException() {
        super(HttpStatus.NOT_FOUND, "Event not found.");
    }



}
