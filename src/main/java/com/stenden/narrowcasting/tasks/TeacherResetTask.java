package com.stenden.narrowcasting.tasks;

import com.stenden.narrowcasting.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TeacherResetTask {

    final TeacherService teacherService;

    @Autowired
    public TeacherResetTask(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    /**
     * Set the the teachers availability to false every night at 0:00AM
     */
    @Scheduled(cron = "0 0 * * * *")
    public void resetTeachers() {
        this.teacherService.resetTeacherAvailability();
    }

}
