package com.stenden.narrowcasting.tasks;

import com.stenden.narrowcasting.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class AccessTokenRemoveTask {

    final AuthenticationService authenticationService;

    @Autowired
    public AccessTokenRemoveTask(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    /**
     * Remove all refresh tokens. This task will be run every month.
     */
    @Scheduled(cron = "0 0 1 * * *")
    void removeAccessTokens() {
        this.authenticationService.removeExpiredAccessTokens();
    }

}
