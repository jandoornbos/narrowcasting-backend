package com.stenden.narrowcasting.controller;

import com.stenden.narrowcasting.advice.ApiException;
import com.stenden.narrowcasting.model.request.AccessTokenRequest;
import com.stenden.narrowcasting.model.response.AccessTokenResponse;
import com.stenden.narrowcasting.service.AuthenticationService;
import com.stenden.narrowcasting.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@Api(tags = "Authenticate")
@RestController
@RequestMapping(value = "/api/v1/auth")
public class AuthenticationController {

    final private AuthenticationService authenticationService;

    @Autowired
    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    /**
     * Authorise a user with the api. The user recieves an access token. This token can
     * be used on other endpoints.
     * @param request The request by the user.
     * @return An access token.
     */
    @ApiOperation(value = "Get an accesstoken")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Obtaining an access token was successful."),
            @ApiResponse(code = 400, message = "Invalid request/Invalid credentials"),
            @ApiResponse(code = 404, message = "User is not found.")
    })
    @RequestMapping(value = "/token", method = RequestMethod.POST)
    public ResponseEntity<AccessTokenResponse> getToken(@RequestBody AccessTokenRequest request) {
        if (request.getGrantType() != null) {
            AccessTokenResponse response;
            if (request.getGrantType().equals("refreshToken")) {
                response = this.authenticationService.refreshToken(request);
            } else {
                response = this.authenticationService.authenticateUser(request);
            }
            return new ResponseEntity<AccessTokenResponse>(response, HttpStatus.OK);
        } else {
            throw new InvalidGrantTypeException();
        }
    }

    private class InvalidGrantTypeException extends ApiException {
        private InvalidGrantTypeException() {
            super(HttpStatus.BAD_REQUEST, "Invalid grant_type.");
        }
    }

}
