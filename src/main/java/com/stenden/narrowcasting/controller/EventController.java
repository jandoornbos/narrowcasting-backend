package com.stenden.narrowcasting.controller;

import com.stenden.narrowcasting.annotation.CurrentUser;
import com.stenden.narrowcasting.annotation.SecuredEndpoint;
import com.stenden.narrowcasting.model.Event;
import com.stenden.narrowcasting.model.User;
import com.stenden.narrowcasting.service.EventService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Api(tags = "Event")
@RequestMapping(value = "/api/v1/event")
public class EventController {

    private EventService eventService;

    public EventController(EventService eventService){
        this.eventService = eventService;
    }

    /**
     * Get all the events that are available to display on the screen
     * Takes an optional ?status parameter
     * If status = 0 this method only returns old events
     * IF status = 1 this method only returns active events
     * @return List of events
     */
    @ApiOperation(value = "Get all events")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Event>> getEvents(@RequestParam (value = "status", required = false) Integer status) {
        // Create events variable
        List<Event> events;
        // Check if the status parameter is given, if so only return requested events, else return all events
        if(status != null && (status == 0 || status == 1)) {
            events = this.eventService.getEventsByStatus(status);
        }
        else {
            events = this.eventService.getEvents();
        }
        return new ResponseEntity<List<Event>>(events, HttpStatus.OK);
    }

    /**
     * Save a new event
     * @param event The new Event to save
     * @return The saved Event
     */
    @SecuredEndpoint
    @ApiOperation(value = "Create an event")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Event> saveEvent(@RequestBody Event event, @CurrentUser User user) {
        event.setCreationDate(new Date());
        event.setCreatedBy(user);
        Event newEvent = this.eventService.saveEvent(event);
        return new ResponseEntity<Event>(newEvent, HttpStatus.CREATED);
    }

    /**
     * Get a single event.
     * @param id The id of the event.
     * @return The event.
     */
    @ApiOperation(value = "Get a single event")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Event> getSingleEvent(@PathVariable int id) {
        Event event = this.eventService.getSingleEvent(id);
        return new ResponseEntity<Event>(event, HttpStatus.OK);
    }

    /**
     * Updates an event
     * @param id The id of the event that needs to be updated
     * @param event The parameter of the event that will be updated
     * @return The event saved in the database
     */
    @SecuredEndpoint
    @ApiOperation(value = "Update an event")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Event> updateEvent(@PathVariable int id, @RequestBody Event event)
    {
        Event newEvent = this.eventService.updateEvent(id, event);
        return new ResponseEntity<Event>(newEvent, HttpStatus.OK);
    }

    /**
     * Delete an event
     * @param id the id of the event to be deleted
     * @return The deleted Event
     */
    @SecuredEndpoint
    @ApiOperation(value = "Delete an event")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteEvent(@PathVariable int id){
        this.eventService.deleteEvent(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
