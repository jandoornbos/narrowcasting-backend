package com.stenden.narrowcasting.controller;

import com.stenden.narrowcasting.model.Teacher;
import com.stenden.narrowcasting.service.TeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Api(tags = "Teacher")
@RequestMapping(value = "/api/v1/teacher")
public class TeacherController {

    private TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    /**
     * Get all teachers to display on the screen.
     * @return List of teachers.
     */
    @ApiOperation(value = "Get all teachers")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Teacher>> getTeachers() {
        List<Teacher> teachers = this.teacherService.getAllTeachers();
        return new ResponseEntity<List<Teacher>>(teachers, HttpStatus.OK);
    }
    /**
     * Get a single teacher, mainly use for the edit page.
     * @param id Pathvariable for the id of the teacher
     * @return Teacher object.
     */
    @ApiOperation(value = "Get a single teacher")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Teacher> getTeacher(@PathVariable int id) {
        Teacher teacher = this.teacherService.getSingleTeacher(id);
        return new ResponseEntity<Teacher>(teacher, HttpStatus.OK);
    }

    /**
     * Create a new teacher.
     * @param teacher The teacher to create.
     * @return The teacher saved in the database.
     */
    @ApiOperation(value = "Create a teacher")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Teacher> saveTeacher(@RequestBody Teacher teacher) {
        Teacher newTeacher = this.teacherService.saveTeacher(teacher);
        return new ResponseEntity<Teacher>(newTeacher, HttpStatus.CREATED);
    }

    /**
     * Update a teacher.
     * @param id The id of the teacher to update.
     * @param teacher The new information of the teacher.
     * @return The teacher updated in the database.
     */
    @ApiOperation(value = "Update a teacher")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Teacher> updateTeacher(@PathVariable int id, @RequestBody Teacher teacher) {
        Teacher updateTeacher = this.teacherService.updateTeacher(id, teacher);
        return new ResponseEntity<Teacher>(updateTeacher, HttpStatus.OK);
    }

    /**
     * Toggle the availability of a teacher.
     * @param id The id of the teacher to update.
     * @return Boolean which states if the operation went well
     */
    @ApiOperation(value = "Toggle availability")
    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<Boolean> toggleTeacher(@PathVariable int id,
                                                 @RequestParam (value = "toggle", required = false) Integer toggle) {
        Boolean success = false;
        if(toggle != null && toggle == 1) {
            // Get the single teacher from the db
            Teacher tTeacher = this.teacherService.getSingleTeacher(id);
            // Invert the availability (true -> false, false -> true)
            tTeacher.setAvailability(!tTeacher.getAvailability());
            // Send the update to the DB
            Teacher updateTeacher = this.teacherService.updateTeacher(id, tTeacher);
            success = true;
        }
        return new ResponseEntity<Boolean>(success, HttpStatus.OK);
    }

    /**
     * Delete a teacher.
     * @param id The id of the teacher to delete.
     * @return The teacher deleted in the database.
     */
    @ApiOperation(value = "Delete a teacher")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Teacher> deleteTeacher(@PathVariable int id) {
        Teacher deleteTeacher = this.teacherService.deleteTeacher(id);
        return new ResponseEntity<Teacher>(deleteTeacher, HttpStatus.OK);
    }
}
