package com.stenden.narrowcasting.controller;

import com.stenden.narrowcasting.annotation.SecuredEndpoint;
import com.stenden.narrowcasting.model.User;
import com.stenden.narrowcasting.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Api(tags = "User")
@RequestMapping(value = "/api/v1/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Get all users.
     *
     * @return All users.
     */
    @SecuredEndpoint
    @ApiOperation(value = "Get all users")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<User>> getUsers() {
        List<User> users = this.userService.getUsers();
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }

    /**
     * Create a new user.
     *
     * @param user The register request.
     * @return The newly created user.
     */
    @SecuredEndpoint
    @ApiOperation(value = "Register a new user")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "A new user has been created.")
    })
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> registerUser(@Valid @RequestBody User user) {
        User created = this.userService.createUser(user);
        return new ResponseEntity<User>(created, HttpStatus.CREATED);
    }

    /**
     * Get a single user.
     *
     * @param id The id of the user.
     * @return The user.
     */
    @SecuredEndpoint
    @ApiOperation(value = "Get a single user")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> getSingleUser(@PathVariable int id) {
        User user = this.userService.getSingleUser(id);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    /**
     * Update an user.
     *
     * @param id   The id of the user to update.
     * @param user The user model.
     * @return The updated user.
     */
    @SecuredEndpoint
    @ApiOperation(value = "Update an user")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable int id, @RequestBody User user) {
        User updatedUser = this.userService.updateUser(id, user);
        return new ResponseEntity<User>(updatedUser, HttpStatus.OK);
    }

    /**
     * Delete an user.
     *
     * @param id The id of the user to delete.
     * @return Nothing.
     */
    @SecuredEndpoint
    @ApiOperation(value = "Delete an user")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUser(@PathVariable int id) {
        this.userService.deleteUser(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
