package com.stenden.narrowcasting.controller;

import com.stenden.narrowcasting.annotation.CurrentUser;
import com.stenden.narrowcasting.annotation.SecuredEndpoint;
import com.stenden.narrowcasting.model.Message;
import com.stenden.narrowcasting.model.User;
import com.stenden.narrowcasting.service.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Api(tags = "Message")
@RequestMapping(value = "/api/v1/message")
public class MessageController {

    private MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    /**
     * Get all the messages that are available to display on the screen
     * Takes an optional ?status parameter
     * If status = 0 this method only returns old messages
     * IF status = 1 this method only returns active messages
     *
     * @return List of messages
     */
    @ApiOperation(value = "Get all messages")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Message>> getMessages(@ApiParam(value = "The status of the message") @RequestParam(value = "status", required = false) Integer status) {
        // Create messages variable
        List<Message> messages;
        // Check if the status parameter is given, if so only return requested events, else return all events
        if (status != null && (status == 0 || status == 1)) {
            messages = this.messageService.getMessagesByStatus(status);
        } else {
            messages = this.messageService.getMessages();
        }
        return new ResponseEntity<List<Message>>(messages, HttpStatus.OK);
    }

    /**
     * Get a single message
     *
     * @param id The id of the message.
     * @return The message.
     */
    @ApiOperation(value = "Get a single message")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Message> getMessage(@PathVariable int id) {
        Message message = this.messageService.getSingleMessage(id);
        return new ResponseEntity<Message>(message, HttpStatus.OK);
    }

    /**
     * Create a new message.
     *
     * @param message The message to create.
     * @return The message saved in the database.
     */
    @SecuredEndpoint
    @ApiOperation(value = "Create a new message")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Message> saveMessage(@RequestBody Message message, @CurrentUser User user) {
        message.setCreationDate(new Date());
        message.setCreatedBy(user);
        Message newMessage = this.messageService.saveMessage(message);
        return new ResponseEntity<Message>(newMessage, HttpStatus.CREATED);
    }

    /**
     * Updates a message.
     *
     * @param id      The id of the message to update.
     * @param message The new content of the message.
     * @return The message saved in the database.
     */
    @SecuredEndpoint
    @ApiOperation(value = "Update a message")
    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<Message> updateMessage(@PathVariable int id, @RequestBody Message message) {
        Message newMessage = this.messageService.updateMessage(id, message);
        return new ResponseEntity<Message>(newMessage, HttpStatus.OK);
    }

    /**
     * Delete a message
     *
     * @param id id of the message to delete
     * @return responseentity
     */
    @SecuredEndpoint
    @ApiOperation(value = "Delete a message")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteMessage(@PathVariable int id) {
        this.messageService.deleteMessage(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}