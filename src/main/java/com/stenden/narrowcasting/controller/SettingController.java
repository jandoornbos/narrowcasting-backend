package com.stenden.narrowcasting.controller;

import com.stenden.narrowcasting.model.Setting;
import com.stenden.narrowcasting.service.SettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Api(tags = "Setting")
@RequestMapping(value = "/api/v1/setting")
public class SettingController {

    final SettingService settingService;

    @Autowired
    public SettingController(SettingService settingService) {
        this.settingService = settingService;
    }

    /**
     * Get all settings.
     * @return A list of Setting.
     */
    @ApiOperation(value = "Get all settings")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Everything okay."),
            @ApiResponse(code = 404, message = "Setting not found.")
    })
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Setting>> getSettings() {
        List<Setting> settings = this.settingService.getSettings();
        return new ResponseEntity<List<Setting>>(settings, HttpStatus.OK);
    }

    /**
     * Get a single setting.
     * @param key The key to search for.
     * @return The setting.
     */
    @ApiOperation(value = "Get a single setting")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Everything okay."),
            @ApiResponse(code = 400, message = "Setting not found.")
    })
    @RequestMapping(value = "/{key}", method = RequestMethod.GET)
    public ResponseEntity<Setting> getSingleSetting(@PathVariable String key) {
        Setting setting = this.settingService.getSettingForKey(key);
        return new ResponseEntity<Setting>(setting, HttpStatus.OK);
    }

    /**
     * Update a setting.
     * @param key The key of the setting to update.
     * @param newSetting The new setting object.
     * @return The updated Setting object.
     */
    @ApiOperation(value = "Update a setting")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Setting updated."),
            @ApiResponse(code = 404, message = "Setting not found."),
            @ApiResponse(code = 400, message = "Invalid Setting object.")
    })
    @RequestMapping(value = "/{key}", method = RequestMethod.PUT)
    public ResponseEntity<Setting> updateSetting(@PathVariable String key, @RequestBody Setting newSetting) {
        Setting setting = this.settingService.updateSettingForKey(key, newSetting);
        return new ResponseEntity<Setting>(setting, HttpStatus.OK);
    }

}
