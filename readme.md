Narrowcasting Backend
=====================

Voor het laten werken van de database moet je een configuratie bestandje plaatsen op de volgende locatie:

`src/main/resources/config/application.properties`

Het bestand moet de volgende inhoud bevatten:

```
db.datasource.url=// jcbd url
db.datasource.username=// username
db.datasource.password=// password
```

Om gebruikers te kunnen registeren op het /register endpoint moet er een secret worden meegegeven.
Deze secret staat ook in de `application.properties` vermeld. De key hiervan is `register.secret`.